/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adatok;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import vezerles.Vezerlo;

/**
 *
 * @author Administrator
 */
public class Uzenet extends Thread {
    public static double kepmeretSzorzo=0.1;
    private Vezerlo vezerlo;
    private BufferedImage emoticon;
    private int emoticonX;
    private int emoticonY;
    private int mozgoX;
    private int mozgoY;
    //private long sleepIdo;
    private int celX;
    private int celY;
    private double lepesSzorzo=0.0000005;
    private BufferedImage smiley;
    private Diak diak;
    private int sorszam;
    
    public Uzenet(Diak diak,BufferedImage emoticon,int emoticonX,int emoticonY,int celX,int celY, Vezerlo vezerlo, BufferedImage smiley) {
        this.diak=diak;
        diak.addUzenetekhez(this);//ekkor kap sorszam-t az üzenet
        this.emoticonX=emoticonX;
        this.mozgoX=emoticonX;
        this.emoticonY=emoticonY;
        this.mozgoY=emoticonY;
        this.emoticon = emoticon;
        this.celX=celX;
        this.celY=celY;
        this.vezerlo=vezerlo;
        this.smiley=smiley;
        
    }

    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        
        double tavolsag=Math.sqrt(Math.pow(emoticonX-celX, 2)+Math.pow(emoticonY-celY, 2));
        double aktualistavolsag=0;
        double lepeshossz=tavolsag*lepesSzorzo;
        mozgoX=(int)(emoticonX+(celX-emoticonX)*(tavolsag*0.01/tavolsag));
        mozgoY=(int)(emoticonY+(celY-emoticonY)*(tavolsag*0.01/tavolsag));
        while(aktualistavolsag<=tavolsag){  
            mozgoX=(int)(emoticonX+(celX-emoticonX)*(aktualistavolsag/tavolsag));
            mozgoY=(int)(emoticonY+(celY-emoticonY)*(aktualistavolsag/tavolsag));
            aktualistavolsag+=lepeshossz;
            frissites();
          //sleep(sleepIdo);  
        }
        if(aktualistavolsag>=tavolsag){
            
            vezerlo.balPanelraUzenet(this);
                    
            emoticon=smiley;
            aktualistavolsag=0;
            while(aktualistavolsag<=tavolsag){
                aktualistavolsag+=lepeshossz;
                mozgoX=(int)(celX+(emoticonX-celX)*(aktualistavolsag/tavolsag));
                mozgoY=(int)(celY+(emoticonY-celY)*(aktualistavolsag/tavolsag));
                frissites();
            }
        }
    }
    
    
    
    public void frissites(){
        vezerlo.frissit();
    }
    
    public void rajzol(Graphics g){
        g.drawImage(emoticon, mozgoX,mozgoY,(int)(emoticon.getWidth()*kepmeretSzorzo),(int)(emoticon.getHeight()*kepmeretSzorzo), null);
    }

    public void setSorszam(int sorszam) {
        this.sorszam=sorszam;
    }

    @Override
    public String toString() {
       return diak.toString()+" "+sorszam+". üzenet.";
    }
    
    
    
}
