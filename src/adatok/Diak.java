/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class Diak {
    private String nev;
    private List<Uzenet> uzenetek=new ArrayList<Uzenet>();
    
    public Diak(String nev) {
        this.nev = nev;
    }
    
    public void addUzenetekhez(Uzenet uzenet){
        uzenetek.add(uzenet);
        uzenet.setSorszam(uzenetek.size());
    }

    @Override
    public String toString() {
        return nev;
    }
    
    
    
}
