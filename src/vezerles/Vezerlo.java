/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vezerles;

import adatok.Diak;
import adatok.Uzenet;
import feluletek.BalPanel;
import feluletek.JobbPanel;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Administrator
 */
public class Vezerlo extends Thread {
    private BalPanel balPanel1;
    private JobbPanel jobbPanel1;
    public List<Diak> diakok;
    public List<Uzenet> uzenetek;
    private final double idoKoz=2.0;
    private List<BufferedImage> emoticonLista;
    private long minimumSleep=500;
    private long maximumSleep=700;
    private BufferedImage smiley;

    public List<Uzenet> getUzenetek() {
        return uzenetek;
    }
    
    public Vezerlo(BalPanel balPanel,JobbPanel jobbPanel){
        this.balPanel1=balPanel;
        this.jobbPanel1=jobbPanel;
        diakokBeolvasas();
        this.smiley=smileyBeolvasas();
    }

    public void diakokBeolvasas() {
        try {
            BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream("src/adatok/diakok.txt")));
            diakok=new ArrayList<Diak>();
            String sor=null;
            while((sor=reader.readLine())!=null){
                diakok.add(new Diak(sor));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
      
            emoticonListaFeltolt();
            uzenetek=new CopyOnWriteArrayList<>();
            int uzenetSzam=0;
            while(uzenetSzam<20){
                try {
                    uzenetSzam++;
                    BufferedImage image=emoticonLista.get((int)(Math.random()*3));
                    int kepSzelesseg=(int)(image.getWidth()*Uzenet.kepmeretSzorzo);
                    int panelSzelesseg=jobbPanel1.getWidth();
                    int kepMelyseg=(int)(image.getHeight()*Uzenet.kepmeretSzorzo);
                    int panelMelyseg=image.getHeight();
                    int emoticonY=kepMelyseg+(int)(Math.random()*(panelMelyseg-2*kepMelyseg));
                    int emoticonX=kepSzelesseg+(int)(Math.random()*(panelSzelesseg-2*kepSzelesseg));
                    int celX=jobbPanel1.getWidth()/2;
                    int celY=0;
                    
                    int randomDiakIndex=(int)(Math.random()*diakok.size());
                    Uzenet uzenet=new Uzenet(diakok.get(randomDiakIndex),image,emoticonX,emoticonY,celX,celY,this,smiley);
                    
                    uzenetek.add(uzenet);
                    uzenet.start();
                    
                    sleep(minimumSleep+((int)(maximumSleep*Math.random())));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
    }
    
    

    public void frissit() {
        jobbPanel1.repaint();
    }
    
    public void rajzol(Graphics g){
        if(uzenetek!=null)
            for(Uzenet uzenet:uzenetek){
                uzenet.rajzol(g);
            }
    }

    private void emoticonListaFeltolt() {
        try {
            BufferedImage emailEmoticon=ImageIO.read(new FileInputStream("src/kepek/email.png"));
            BufferedImage facebookEmoticon=ImageIO.read(new FileInputStream("src/kepek/facebook.png"));
            BufferedImage messengerEmoticon=ImageIO.read(new FileInputStream("src/kepek/messenger.png"));
            emoticonLista=new ArrayList<>();
            emoticonLista.add(emailEmoticon);
            emoticonLista.add(facebookEmoticon);
            emoticonLista.add(messengerEmoticon);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private BufferedImage smileyBeolvasas(){
        try {
            smiley=ImageIO.read(new FileInputStream("src/kepek/smiley.png"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return smiley;
    }

    public void balPanelraUzenet(Uzenet uzenet) {
       balPanel1.addUzenet(uzenet);
    }

    public void uzenetekLeallitasa() {
       /*for(Uzenet uzenet:uzenetek){
           try {
               uzenet.join();
           } catch (InterruptedException ex) {
               Logger.getLogger(Vezerlo.class.getName()).log(Level.SEVERE, null, ex);
           }
       }*/
    }
    
}
