/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import feluletek.BalPanel;
import feluletek.JobbPanel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import vezerles.Vezerlo;

/**
 *
 * @author Administrator
 */
public class DiakListaTest {
    
    public DiakListaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void diakLista() {
        Vezerlo vezerlo=new Vezerlo(new BalPanel(),new JobbPanel());
        assertNotNull(vezerlo.diakok);
        assertEquals(vezerlo.diakok.size(), 30);
    }
}
